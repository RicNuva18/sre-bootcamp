# Compose


"Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration. To learn more about all the features of Compose."



## Containers on EC2 (primer)

Install Docker 🐳 and docker-compose

```
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh


sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

```

### Start the application


Using docker-compose 🔥

```
# render the docker-compose.yml file
docker-compose config

# build the images
docker-compose build

# start the containers
docker-compose up
```


Using docker cmds 👎

```
# not recommended
docker image build -f sql/mssql/Dockerfile -t mssql-server:latest sql/mssql
docker image build -f app/Dockerfile -t app:latest app

docker network create --driver bridge myapp

docker container run --network=myapp --rm -d --env-file=app/.env.example --name db mssql-server:latest
docker container run -p 8080:5000 --network=myapp -it --rm --env-file=app/.env.example --name app app:latest
```

---

# [Compose + ECS](https://docs.docker.com/cloud/ecs-integration/)

```

export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_DEFAULT_REGION=us-east-1

docker context create ecs myecscontext
docker context use myecscontext

docker compose up --context myecscontext --build
```

# Furher reading

- [Docker Swarm](https://docs.docker.com/compose/swarm/)
- [Docker+ ECS](https://docs.aws.amazon.com/AmazonECS/latest/userguide/docker-basics.html)
- [ECS Copilot](https://docs.aws.amazon.com/AmazonECS/latest/userguide/getting-started-aws-copilot-cli.html)



